# CHANGELOG

<!--- next entry here -->

## 0.2.0
2020-08-11

### Features

- final revision (0109c7083678e8d5f0e8cc3f2e55d0151b45d2cd)

## 0.1.0
2020-03-11

### Features

- Update policy.md (030f29588a0ae21ae493c9773a72cc048e627370)